def valid_words(data_file: str) -> list:
    for line in open(data_file):
        words = [word for word in line.strip().split()]
    return words    